package Tests.Localidades;

import org.junit.Test;

import PageActions.PageLocalidade;
import PageActions.PageLogin;
import PageActions.PagePrincipal;
import Setup.Setup;

public class CancelarCadastroLocalidade extends Setup {

	PageLogin login = new PageLogin();
	PagePrincipal principal = new PagePrincipal();
	PageLocalidade local = new PageLocalidade();

	@Test
	public void cancelarCadastroLocalidade() {

		login.preencherCampoEmail();
		login.preencherCampoSenha();
		login.clicarBtnEntrar();
		principal.clicarLocalidade();
		local.clicarAddLocalidade();
		local.preencherCampoNome();
		local.clicarBtnCancelar();
		local.validarCancelamentoLocalidade();

	}

}
