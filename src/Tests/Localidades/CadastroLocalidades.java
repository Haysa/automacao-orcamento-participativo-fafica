package Tests.Localidades;

import org.junit.Test;

import PageActions.PageLocalidade;
import PageActions.PageLogin;
import PageActions.PagePrincipal;
import Setup.Setup;

public class CadastroLocalidades extends Setup {

	@Test
	public void cadastrarLocalidade() {

		PageLogin login = new PageLogin();
		PagePrincipal principal = new PagePrincipal();
		PageLocalidade localidade = new PageLocalidade();

		login.preencherCampoEmail();
		login.preencherCampoSenha();
		login.clicarBtnEntrar();

		for (int i = 0; i < 50; i++) {
			principal.clicarLocalidade();
			localidade.clicarAddLocalidade();
			localidade.preencherCampoNome();
			localidade.selecionarZonaUrbana();
			localidade.clicarBtnConcluir();
			

		}
	}
}
