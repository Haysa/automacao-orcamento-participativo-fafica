package Tests.Rop;

import org.junit.Test;

import PageActions.PageLogin;
import PageActions.PagePrincipal;
import PageActions.PageRop;
import Setup.Setup;

public class CadastroDeRop extends Setup {

	@Test
	public void cadastrarRop() {
		PageLogin login = new PageLogin();
		PagePrincipal principal = new PagePrincipal();
		PageRop rop = new PageRop();

		login.preencherCampoEmail();
		login.preencherCampoSenha();
		login.clicarBtnEntrar();
		principal.clicarRop();
		rop.clicarAddRop();
		rop.preencherCampoNome();
		rop.escolherLocalidade("l");
		rop.escolherCiclo("2015");
		rop.clicarConcluir();

	}

}
