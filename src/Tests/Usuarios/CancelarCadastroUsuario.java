package Tests.Usuarios;

import org.junit.Test;

import PageActions.PageLogin;
import PageActions.PagePrincipal;
import PageActions.PageUsuarios;
import Setup.Setup;

public class CancelarCadastroUsuario extends Setup {

	PageLogin login = new PageLogin();
	PagePrincipal principal = new PagePrincipal();
	PageUsuarios usuario = new PageUsuarios();

	@Test
	public void cancelarCadastroUsuario() {
		
		login.preencherCampoEmail();
		login.preencherCampoSenha();
		login.clicarBtnEntrar();
		
		principal.clicarUsuario();	
		
		usuario.clicarAddUsuario();
		usuario.preencherCampoNome();
		usuario.preencherCampoSobreNome();
		usuario.clicarCancelar();
		usuario.validarCancelamentoDeCadastroUsuario();
		
		
		
	}

}
