package Tests.Usuarios;

import org.junit.Test;

import PageActions.PageLogin;
import PageActions.PagePrincipal;
import PageActions.PageUsuarios;
import Setup.Setup;

public class CadastroDeUsuariosCamposEmBranco extends Setup {

	@Test
	public void cadastroUsuarioComCamposEmBranco() {
		
		PageLogin login = new PageLogin();
		PagePrincipal principal = new PagePrincipal();
		PageUsuarios usuario = new PageUsuarios();
		
		login.preencherCampoEmail();
		login.preencherCampoSenha();
		login.clicarBtnEntrar();
		
		principal.clicarUsuario();
		
		usuario.clicarConcluir();
	}

}
