package Tests.Usuarios;

import org.junit.Test;

import PageActions.PageLogin;
import PageActions.PagePrincipal;
import PageActions.PageUsuarios;
import Setup.Setup;

public class CadastroDeUsuarios extends Setup {

	@Test
	public void cadastrarUsuarios() throws InterruptedException {

		PageLogin login = new PageLogin();
		PagePrincipal principal = new PagePrincipal();
		PageUsuarios user = new PageUsuarios();

		login.preencherCampoEmail();
		login.preencherCampoSenha();
		login.clicarBtnEntrar();

		/*for (int i = 0; i < 100; i++) {*/
			principal.clicarUsuario();
			user.clicarAddUsuario();
			user.preencherCampoNome();
			user.preencherCampoSobreNome();
			user.preencherCampoSenha();
			user.preencherCampoConfirmaSenha();
			user.preencherCampoEmail();
			user.clicarConcluir();
			user.validarCadastroUsuario();

		}

	}

