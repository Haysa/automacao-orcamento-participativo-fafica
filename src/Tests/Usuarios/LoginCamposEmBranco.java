package Tests.Usuarios;

import org.junit.Test;

import PageActions.PageLogin;
import Setup.Setup;

public class LoginCamposEmBranco extends Setup {

	@Test
	public void loginComCamposEmBranco() {
		PageLogin login = new PageLogin();
		
		login.clicarBtnEntrar();
		login.validarMsgsCamposObrigatoriosEmail();
		login.validarMsgsCamposObrigatoriosSenha();
	}

}
