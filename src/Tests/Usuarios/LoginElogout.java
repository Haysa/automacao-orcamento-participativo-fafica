package Tests.Usuarios;

import org.junit.Test;

import PageActions.PageLogin;
import Setup.Setup;

public class LoginElogout extends Setup {

	@Test
	public void realizarLoginElogout() {
		PageLogin login = new PageLogin();
		
		login.preencherCampoEmail();
		login.preencherCampoSenha();
		login.clicarBtnEntrar();
		login.clicarOpcaoSair();
		login.clicarBtnLogout();
		login.validarLogout();
	}

}
