package Setup;

import java.util.concurrent.TimeUnit;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Setup {

	public static WebDriver selenium;

	@BeforeClass
	public static void setup() {

		selenium = new FirefoxDriver();
		selenium.manage().timeouts().implicitlyWait(2000, TimeUnit.SECONDS);
		selenium.manage().timeouts().pageLoadTimeout(2000, TimeUnit.SECONDS);
		selenium.get("https://teste.fafica-pe.edu.br/orcamento_participativo/login/");
		selenium.manage().window().maximize();

	}

	@AfterClass
	public static void tearDown() {

		/*elenium.quit();*/
	}
}
