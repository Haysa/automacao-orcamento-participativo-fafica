package PageActions;

import static org.testng.AssertJUnit.assertEquals;
import org.openqa.selenium.By;

import Setup.Setup;
import Utils.GeradorDeNomes;

public class PageUsuarios extends Setup {

	static By adicionarUsuario = By
			.xpath("html/body/div[2]/div[2]/div/div[2]/div/div/div[1]/div[2]/a");
	static By campoNome = By.name("nome");
	static By campoSobreNome = By.name("sobrenome");
	static By campoSenha = By.name("senha");
	static By campoConfirmaSenha = By.name("rsenha");
	static By campoEmail = By.name("email");
	static By btnConcluir = By
			.xpath(".//*[@id='form-usuario']/div/div[2]/button");
	static By btnCancelar = By.xpath(".//*[@id='form-usuario']/div/div[2]/a");
	static By alertSucessoUsuario = By
			.xpath("html/body/div[2]/div[2]/div/div[2]/div/div/div[2]/div[1]");
	static By textoValidarCancelamento = By
			.xpath("html/body/div[2]/div[2]/div/div[2]/div/div/div[1]/div[2]/a");

	public void clicarAddUsuario() {
		selenium.findElement(adicionarUsuario).click();
	}

	public void preencherCampoNome() {
		selenium.findElement(campoNome).sendKeys(GeradorDeNomes.gerarNome());
	}

	public void preencherCampoSobreNome() {
		selenium.findElement(campoSobreNome).sendKeys(
				GeradorDeNomes.gerarNome());
	}

	public void preencherCampoSenha() {
		String senha = "123";
		selenium.findElement(campoSenha).sendKeys(senha);
	}

	public void preencherCampoConfirmaSenha() {
		String senha = "123";
		selenium.findElement(campoConfirmaSenha).sendKeys(senha);
	}

	public void preencherCampoEmail() {
		selenium.findElement(campoEmail).sendKeys(
				GeradorDeNomes.gerarNome() + "@teste.com");
	}

	public void clicarConcluir() {
		selenium.findElement(btnConcluir).click();
	}

	public void clicarCancelar() {
		selenium.findElement(btnCancelar).click();
	}

	public void validarCadastroUsuario() {
		String h = selenium.findElement(alertSucessoUsuario).getText();
		assertEquals("usu�rio cadastrado com sucesso", h);
		System.out.println(h);
	}

	public void validarCancelamentoDeCadastroUsuario() {
		String g = selenium.findElement(textoValidarCancelamento).getText();
		assertEquals("Adicionar Usu�rio", g);
		System.out.println(g);

	}
}
