package PageActions;

import static org.testng.AssertJUnit.assertEquals;

import org.openqa.selenium.By;

import Setup.Setup;

public class PageLogin extends Setup {

	static By campoEmail = By
			.xpath("html/body/div[2]/form/div[1]/div/div/input");
	static By campoSenha = By.name("senha");
	static By btnEntrar = By.xpath("html/body/div[2]/form/div[3]/button");
	static By comboSair = By.xpath("html/body/div[1]/div/div/ul/li/a/i");
	static By btnLogout = By.xpath("html/body/div[1]/div/div/ul/li/ul/li/a");
	static By textoValidaLogout = By.xpath("html/body/div[2]/form/h3");
	static By textoValidaCamposObg1 = By
			.xpath("html/body/div[2]/form/div[1]/div/label");
	static By textoValidaCamposObg2 = By
			.xpath("html/body/div[2]/form/div[2]/div/label");

	public void preencherCampoEmail() {
		selenium.findElement(campoEmail).sendKeys("teste@teste.com");

	}

	public void preencherCampoSenha() {
		selenium.findElement(campoSenha).sendKeys("123");
	}

	public void clicarBtnEntrar() {
		selenium.findElement(btnEntrar).click();
	}

	public void clicarOpcaoSair() {
		selenium.findElement(comboSair).click();
	}

	public void clicarBtnLogout() {
		selenium.findElement(btnLogout).click();
	}

	public void validarLogout() {
		String h = selenium.findElement(textoValidaLogout).getText();
		assertEquals("Autentica��o de Usu�rio", h);
		System.out.println(h);
	}

	public void validarMsgsCamposObrigatoriosEmail() {
		String h = selenium.findElement(textoValidaCamposObg1).getText();
		assertEquals("Este campo � obrigat�rio.", h);
		System.out.println(h);

	}

	public void validarMsgsCamposObrigatoriosSenha() {
		String g = selenium.findElement(textoValidaCamposObg2).getText();
		assertEquals("Este campo � obrigat�rio.", g);
		System.out.println(g);

	}
}
