package PageActions;

import static org.testng.AssertJUnit.assertEquals;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import Setup.Setup;
import Utils.GeradorDeNomes;

public class PageLocalidade extends Setup {

	static By addLocalidade = By
			.xpath("html/body/div[2]/div[2]/div/div[2]/div/div/div[1]/div[2]/a");
	static By campoNome = By.name("nome");
	static By comboBoxZonaRuralOuUrbana = By.name("zona");
	static By btnConcluir = By
			.xpath(".//*[@id='form-localidade']/div[5]/button");
	static By btnCancelar = By.xpath(".//*[@id='form-localidade']/div[5]/a");
	static By textoValidarCancelar = By
			.xpath("html/body/div[2]/div[2]/div/div[2]/div/div/div[1]/div[2]/a");

	public void clicarAddLocalidade() {
		selenium.findElement(addLocalidade).click();
	}

	public void preencherCampoNome() {
		selenium.findElement(campoNome).sendKeys(GeradorDeNomes.gerarNome());

	}

	public void selecionarZonaUrbana() {
		selenium.findElement(comboBoxZonaRuralOuUrbana).sendKeys("Zona Urbana");

	}

	public void clicarBtnConcluir() {
		selenium.findElement(btnConcluir).click();
	}

	public void clicarBtnCancelar() {
		selenium.findElement(btnCancelar).click();
	}

	public void validarCadastroLocalidade() {
	}

	public void validarCancelamentoLocalidade() {
		String f = selenium.findElement(textoValidarCancelar).getText();
		assertEquals("Adicionar Localidade", f);
		System.out.println(f);
	}

}
