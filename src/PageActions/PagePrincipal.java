package PageActions;

import org.openqa.selenium.By;

import Setup.Setup;

public class PagePrincipal extends Setup {

	static By clicarUsuario = By
			.xpath("html/body/div[2]/div[1]/ul/li[3]/a/span");
	static By clicarLocalidade = By
			.xpath("html/body/div[2]/div[1]/ul/li[4]/a/span");
	static By clicarRop = By.xpath("html/body/div[2]/div[1]/ul/li[5]/a");

	public void clicarUsuario() {

		selenium.findElement(clicarUsuario).click();
	}

	public void clicarLocalidade() {
		selenium.findElement(clicarLocalidade).click();

	}

	public void clicarRop() {
		selenium.findElement(clicarRop).click();
	}
}
