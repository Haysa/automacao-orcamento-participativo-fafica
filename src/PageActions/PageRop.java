package PageActions;

import org.openqa.selenium.By;

import Setup.Setup;
import Utils.GeradorDeNomes;

public class PageRop extends Setup {

	static By btnAddRop = By
			.xpath("html/body/div[2]/div[2]/div/div[2]/div/div/div[1]/div[2]/a");
	static By campoNome = By.name("nome");
	static By campoLocalidade = By.xpath(".//*[@id='rop_localidades']");
	static By campoCiclo = By.name("ciclo");
	static By btnConcluir = By.xpath(".//*[@id='form-rop']/div[6]/button");
	static By btnCancelar = By.xpath(".//*[@id='form-rop']/div[6]/a");

	public void clicarAddRop() {
		selenium.findElement(btnAddRop).click();
	}

	public void preencherCampoNome() {
		selenium.findElement(campoNome).sendKeys(GeradorDeNomes.gerarNome());
	}

	public void escolherLocalidade(String loc) {
		selenium.findElement(campoLocalidade).sendKeys(loc);
	}

	public void escolherCiclo(String ciclo) {
		selenium.findElement(campoCiclo).sendKeys(ciclo);
	}

	public void clicarConcluir() {
		selenium.findElement(btnConcluir).click();
	}

	public void clicarCancelar() {
		selenium.findElement(btnCancelar).click();
	}
}
